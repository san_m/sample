/**
 * 
 */

var cart = [];

var restaurantObj = {};

var userObj = {};

var orderNo = "";

function init(){
	document.getElementById("orderDetailsHolder").innerHTML = "";
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200 && validateServerResponse(this.responseText)) {
			userObj = JSON.parse(this.responseText);
			document.getElementById("userAddr").innerHTML=userObj.userAddress; 
			loadDestinationAddresses();
		}
	};
	xhttp.open("GET", "/Sample/rest/userController/user/0", true);
	xhttp.send();
}

function loadDestinationAddresses() {
	//Load Destination combo
	var xhttp = new XMLHttpRequest();
	var selectComboval;
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200 && validateServerResponse(this.responseText)) {
			selectComboval = JSON.parse(this.responseText);
			var select = document.getElementById("restaurants");
			var nearByAddresses = selectComboval.nearByAddresses;

			for(var i = 0; i < nearByAddresses.length; i++) {
				var nearByAddress = nearByAddresses[i];
				var option = document.createElement("option");
				option.text = nearByAddress.addressName;
				option.value = nearByAddress.addressId;
				select.appendChild(option);
			}
		}
		loadRestaurantDetails();
	};
	xhttp.open("GET", "/Sample/rest/addressController/getNearAddress/"+userObj.userAddressId, true);
	xhttp.send();
}

function loadRestaurantDetails(){
	var key = document.getElementById("restaurants").value;
	var priceComVal = document.getElementById("priceLimit").value;
	var ratingComVal = document.getElementById("ratingLimit").value;

	this.cart = [];
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200 && validateServerResponse(this.responseText)) {
			restaurantObj = JSON.parse(this.responseText);
			var select = document.getElementById("resTable");
			select.innerHTML = "";

			var hearderNames = ["Restaurant Name","Rating","Dish Name","Price"," ",];

			//Add the header row.
			var row = select.insertRow(-1);
			for (var i = 0; i <= 4; i++) {
				var headerCell = document.createElement("TH");
				headerCell.innerHTML = hearderNames[i];
				row.appendChild(headerCell);
			}

			var priceFilteredObj = filterByPrice(restaurantObj,priceComVal);
			var ratingFilterObj = filterByRating(restaurantObj,ratingComVal);

			var filerObj = [];
			var j=-1;
			if(priceComVal != '' || ratingComVal != '' ){
				for (var i = 0; i < restaurantObj.length; i++) {
					var price = restaurantObj[i]["price"];
					var rating = restaurantObj[i]["rating"];
					if(priceComVal != '' && ratingComVal != ''){
						if(price <= priceComVal && rating <= ratingComVal){
							j++;
							filerObj[j]=restaurantObj[i];
						}
					}else if(ratingComVal != ''){
						if(rating <= ratingComVal ){
							j++;
							filerObj[j]=restaurantObj[i];
						}
					}else if(priceComVal != ''){
						if(price <= priceComVal){
							j++;
							filerObj[j]=restaurantObj[i];
						}
					}
				}
			}else{
				filerObj= restaurantObj;
			}

			// ADD JSON DATA TO THE TABLE AS ROWS.
			for (var i = 0; i < filerObj.length; i++) {
				tr = select.insertRow(-1);
				var tabCell1 = tr.insertCell(-1);
				tabCell1.innerHTML = filerObj[i]["restaurantName"];
				var tabCell2 = tr.insertCell(-1);
				var ratingVal = filerObj[i]["rating"];
				var remaining = 5-ratingVal;
				for(var j=1;j<=ratingVal ;j++){
					var spanStarEle = document.createElement("span");
					spanStarEle.setAttribute("class", "fa fa-star checked")
					tabCell2.appendChild(spanStarEle);
				}
				for(var j=1;j<=remaining ;j++){
					var spanStarEle = document.createElement("span");
					spanStarEle.setAttribute("class", "fa fa-star")
					tabCell2.appendChild(spanStarEle);
				}
				var tabCell3 = tr.insertCell(-1);
				tabCell3.innerHTML = filerObj[i]["dishName"];
				var tabCell4 = tr.insertCell(-1);
				tabCell4.innerHTML = "Rs."+filerObj[i]["price"];
				var tabCell5 = tr.insertCell(-1);
				var qtyInput = document.createElement("input");
				qtyInput.type="number";
				qtyInput.id="qty_"+filerObj[i]["restaurantid"] + "_" + filerObj[i]["dishid"];
				qtyInput.min="1";
				qtyInput.qty="50";
				qtyInput.style="width:30px";
				qtyInput.value=1;
				qtyInput.addEventListener("change",changeQty);
				qtyInput.addEventListener("blur",validateQty);
				tabCell5.appendChild(qtyInput);
				var addbtn = document.createElement("button");
				addbtn.innerText = "Add";
				addbtn.id = "addbtn_" + filerObj[i]["restaurantid"] + "_" + filerObj[i]["dishid"];
				addbtn.addEventListener("click", addItem);
				tabCell5.appendChild(addbtn);
			}
		}
	};
	xhttp.open("GET", "/Sample/rest/restaurantController/getRestaurantsInfo/"+key, true);
	xhttp.send();
	validateAndEnableOrderButton();
}

function searchByGrid(event){
	loadRestaurantDetails()
}

function filterByPrice(obj,priceComVal){
	var priceFilterdObj = [];
	var j=-1;
	if(priceComVal != ''){
		for (var i = 0; i < obj.length; i++) {
			var price = obj[i]["price"];
			if(price <= priceComVal){
				j++;
				priceFilterdObj[j]=restaurantObj[i];
			}
		}
	}else{
		priceFilterdObj = restaurantObj;
	}
}

function filterByRating(obj,ratingComVal){
	var ratingFilterdObj = [];
	var j=-1;
	if(ratingComVal != ''){
		for (var i = 0; i < obj.length; i++) {
			var rating = restaurantObj[i]["rating"];
			if(rating <= ratingComVal){
				j++;
				ratingFilterdObj[j]=restaurantObj[i];
			}
		}
	}else{
		ratingFilterdObj = restaurantObj;
	}
}

function addItem(event){
	var res = event.currentTarget.id;
	var restaurantId = res.split("_")[1];
	var dishId = res.split("_")[2];
	//fetch Qty value
	var qty = document.getElementById("qty_"+restaurantId+"_"+dishId).value;

	if(event.currentTarget.innerText == "Add"){
		var actOnObj = fetchObjDetail(restaurantId, dishId);
		var cartItem = {"item":actOnObj, "quantity":qty}
		addToCart(cartItem);
		event.currentTarget.innerText = "Delete";
	}else if(event.currentTarget.innerText == "Delete"){
		var objectToRemove = findTargetRowFromCart(restaurantId, dishId);
		removeFromCart(objectToRemove);
		event.currentTarget.innerText = "Add";
	}
}

function changeQty(event){
	var qtyId = event.currentTarget.id;
	var changedQty = document.getElementById(qtyId).value;
	if(changeQty <= 0){
		document.getElementById(qtyId).value = 1;
	}

	var restaurantId = qtyId.split("_")[1];
	var dishId = qtyId.split("_")[2];
	var btnText = 	document.getElementById("addbtn_"+restaurantId+"_"+dishId).innerText;
	if(btnText == "Delete"){
		var targetObj =findTargetRowFromCart(restaurantId,dishId)
		targetObj["quantity"] = changedQty;
		validateAndEnableOrderButton();
	}

}

function findTargetRowFromCart(restaurantId,dishId){
	for (var i = 0; i < this.cart.length; i++) {
		if(this.cart[i]["item"]["restaurantid"] == restaurantId &&  this.cart[i]["item"]["dishid"] == dishId){
			return this.cart[i];
		}
	}
}

function fetchObjDetail(restaurantId,dishId){
	for (var i = 0; i < restaurantObj.length; i++) {
		if(restaurantObj[i]["restaurantid"] == restaurantId &&  restaurantObj[i]["dishid"] == dishId){
			return restaurantObj[i];
		}
	}
}

function addToCart(target){
	this.cart.push(target);
	validateAndEnableOrderButton();
}

function removeFromCart(target){
	var index = this.cart.indexOf(target);
	if(index > -1) {
		this.cart.splice(index, 1);
	}
	validateAndEnableOrderButton();
}

function validateAndEnableOrderButton() {
	var price = 0;
	if(this.cart.length <= 0){
		document.getElementById("totalPrice").innerText="";
		document.getElementById("orderButton").disabled = true;
		document.getElementById("orderButton").style.backgroundColor="#73AA73";
	}else{
		for(var i=0;i<this.cart.length;i++){
			price += this.cart[i]["item"]["price"]*this.cart[i]["quantity"];
		}
		document.getElementById("totalPrice").innerText="Total Price :  Rs."+price;
		document.getElementById("orderButton").disabled = false;
		document.getElementById("orderButton").style.backgroundColor="#4CAF50";
	}
}

function openOrderCreationDlg(){
	var orderInfo={};
	orderInfo["cart"] = this.cart;
	orderInfo["user"]= this.userObj;

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200 && validateServerResponse(this.responseText)) {
			var resposne = JSON.parse(this.responseText);
			this.orderNo = resposne["OrderNo"];
			document.getElementById("orderDlg").open = true;
			document.getElementById("mainpage").remove();
		}
	}
	xhttp.open("POST", "/Sample/rest/OrderController/placeOrder", true);
	xhttp.send(JSON.stringify(orderInfo));
}

function viewOrderDetails(){
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200 && validateServerResponse(this.responseText)) {
			var price = 0;
			document.getElementById("fieldSetId").style="display:block";
			document.getElementById("viewOrderBtn").remove();
			var fieldSetElement = document.getElementById("orderDetailsHolder");
			for(var i=0;i<cart.length;i++){
				price += cart[i]["item"]["price"]*cart[i]["quantity"];
				var labelCom = document.createElement("label");
				var breakEle = document.createElement("br");
				labelCom.innerText = (i+1) + " ) "+cart[i]["item"]["dishName"] + " - Qty " + cart[i]["quantity"];
				fieldSetElement.appendChild(labelCom);
				fieldSetElement.appendChild(breakEle);
			}
			var totalPricelabel = document.createElement("label");
			totalPricelabel.innerHTML = "<br> <br> Total Price :  Rs."+price;
			fieldSetElement.appendChild(totalPricelabel);
		}
	}
	xhttp.open("POST", "/Sample/rest/OrderController/getOrderInfo", true);
	xhttp.send(JSON.stringify(orderNo));
}

function validatePriceRange(event) {
	var priceLimit = event.value;
	if(priceLimit <= 100){
		event.value = 100;
	} else if(priceLimit >= 500) {
		event.value = 500;
	}	
}

function validateRating(event) {
	var rating = event.value;
	if(rating < 0) {
		event.value = "";
	} else if(rating >= 5) {
		event.value = 5;
	} 
}

function validateQty(event){
	var qtyId = event.currentTarget.id;
	var changedQty = document.getElementById(qtyId).value;
	if(changedQty <= 0){
		document.getElementById(qtyId).value=1;
	}
}

function validateServerResponse(response) {
	var parsedResponse = JSON.parse(response);
	var status = parsedResponse["status"];
	if(typeof status === "undefined" || status != "Error"){
		return true;
	} else {
		var message = parsedResponse["message"];
		document.getElementById("errorbanner").innerHTML = message;
		document.getElementById("errorbanner").style = "display:block";
		document.getElementById("errorbanner").focus();
		return false;
	}
}

