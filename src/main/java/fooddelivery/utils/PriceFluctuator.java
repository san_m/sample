package fooddelivery.utils;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.format.TextStyle;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import fooddelivery.entity.Price;

public class PriceFluctuator {
	
	public enum IntervalType {
		BreakFast, Lunch, Dinner
	};
	
	private static final String TIME_INTERVAL = "Interval";
	private static final String PRICE_FACTOR = "Factor";
	
	private static final String PROPERTY_SEPERATOR = ".";
	
	private DynamicConfigReader dynamicConfigReader;
	private LocalDateTime localDateTime;
	
	//Useful for testing in particular time
	public PriceFluctuator(DynamicConfigReader dynamicConfigReader, Date date, LocalDateTime localDateTime) {
		this.dynamicConfigReader = dynamicConfigReader;
		this.localDateTime = localDateTime;
	}
	
	public PriceFluctuator(DynamicConfigReader dynamicConfigReader) {
		this.dynamicConfigReader = dynamicConfigReader;
	}
	
	public PriceFluctuator() {
		String path = getClass().getClassLoader().getResource("price_fluctuator.properties").getPath();
		this.dynamicConfigReader = new DynamicConfigReader(path);
	}
	
	public List<Price> flutuatePrice(List<Price> priceList){
		try {
			LocalDateTime now = this.localDateTime;
			if(now == null){
				now = LocalDateTime.now();
			}
			
			String dayOfWeek = retrieveDayOfWeek(now);
			String intervalType = retrieveIntervalTyep(now);
			if(isNullOrBlank(dayOfWeek) || isNullOrBlank(intervalType)) {
				return priceList;
			}
			double priceFactor = fetchPriceFactor(dayOfWeek, intervalType);
			
			priceList.forEach(price -> price.setPrice(price.getPrice()*priceFactor));
			
			return priceList;
		} catch (Exception exp) {
			exp.printStackTrace();
		}
		return priceList;
	}
	
	private String retrieveDayOfWeek(LocalDateTime now) {
		DayOfWeek dow = now.getDayOfWeek();
		return dow.getDisplayName(TextStyle.FULL, Locale.getDefault());
	}
	
	private String retrieveIntervalTyep(LocalDateTime now) throws Exception{
		String[] breakFastInerval = fetchIntervalTimeFor(IntervalType.BreakFast.toString());
		String[] lunchInerval = fetchIntervalTimeFor(IntervalType.Lunch.toString());
		String[] dinnerInterval = fetchIntervalTimeFor(IntervalType.Dinner.toString());	
		
		int hourOfDay = now.getHour();
		String intervalType = "";
		if (inInterval(breakFastInerval, hourOfDay) ) {
			intervalType = IntervalType.BreakFast.toString();
		} else if (inInterval(lunchInerval, hourOfDay) ) {
			intervalType = IntervalType.Lunch.toString();
		} else if (inInterval(dinnerInterval, hourOfDay) ) {
			intervalType = IntervalType.Dinner.toString();
		}
		return intervalType;
	}
	
	private String[] fetchIntervalTimeFor(String key) throws Exception{
		String intervalStr = this.dynamicConfigReader.readProperty(key 
				+ PROPERTY_SEPERATOR 
				+ TIME_INTERVAL);
		if(isNullOrBlank(intervalStr)){
			throw new Exception("No config found");
		}
		return intervalStr.split(",");
	}
	
	private boolean inInterval(String[] interval, int hourOfDay) {
		int leftBound = Integer.parseInt(interval[0]);
		int rightBound = Integer.parseInt(interval[1]);
		if(leftBound <= hourOfDay && hourOfDay < rightBound) {
			return true;
		}
		return false;
	}
	
	private double fetchPriceFactor(String dayOfWeek, String intervalType) throws Exception {
		String priceFactorStr = this.dynamicConfigReader.readProperty(dayOfWeek + PROPERTY_SEPERATOR
																	  + intervalType + PROPERTY_SEPERATOR
																	  + PRICE_FACTOR);
		if(isNullOrBlank(priceFactorStr)){
			throw new Exception("No config found");
		}
		double factor = 1.0;
		try{
			factor = Double.parseDouble(priceFactorStr);
		}catch(Exception exp){
			exp.printStackTrace();
		}
		return factor;
	}
	
	private boolean isNullOrBlank(String str) {
		return null == str || "".equals(str);
	}

	public DynamicConfigReader getDynamicConfigReader() {
		return dynamicConfigReader;
	}

	public void setDynamicConfigReader(DynamicConfigReader dynamicConfigReader) {
		this.dynamicConfigReader = dynamicConfigReader;
	}

	public LocalDateTime getLocalDateTime() {
		return localDateTime;
	}

	public void setLocalDateTime(LocalDateTime localDateTime) {
		this.localDateTime = localDateTime;
	}
}
