package fooddelivery.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class DynamicConfigReader {
	
	private Properties properties;
	private String fileName;
	
	public DynamicConfigReader(String fileName){
		this.properties = new Properties();
		this.fileName = fileName;
	}
	
	public DynamicConfigReader(Properties properties) {
		this.properties = properties;
	}
	
	public String readProperty(String key) throws IOException {
		if(null != fileName){
			FileInputStream fis = new FileInputStream(fileName);
			this.properties.load(fis);
		}
		return this.properties.getProperty(key);
	}
}
