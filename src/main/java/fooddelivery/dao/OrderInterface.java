package fooddelivery.dao;

import java.util.List;

import fooddelivery.entity.Order;
import fooddelivery.entity.OrderDetail;

public interface OrderInterface {
	
	public String saveOrder(Order orderInfo);
	
	public void saveOrderDetail(OrderDetail orderDetailInfo);
	
	public List<OrderDetail> getOrderInfo(String orderNo);

}
