package fooddelivery.dao;

import java.util.List;

import fooddelivery.entity.Dish;

public interface DishInterface {
	
	public Dish getDish(int dishId);

	public List<Dish> getDishDetails(List<Integer> dishIdlist);

}
