package fooddelivery.dao.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FailureResponseDto {
	private String status;
	private String message;
	
	@JsonCreator
	public FailureResponseDto(@JsonProperty("status") String status, 
			@JsonProperty("message") String message) {
		super();
		this.status = status;
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
