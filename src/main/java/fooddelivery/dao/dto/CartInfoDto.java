package fooddelivery.dao.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CartInfoDto {
	private RestaurantInfoDto restaurantInfo;
	private int quantity;
	
	@JsonCreator
	public CartInfoDto(@JsonProperty("item")RestaurantInfoDto restaurantInfo, 
					   @JsonProperty("quantity") int quantity) {
		super();
		this.restaurantInfo = restaurantInfo;
		this.quantity = quantity;
	}

	public RestaurantInfoDto getRestaurantInfo() {
		return restaurantInfo;
	}

	public void setRestaurantInfo(RestaurantInfoDto restaurantInfo) {
		this.restaurantInfo = restaurantInfo;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	
}
