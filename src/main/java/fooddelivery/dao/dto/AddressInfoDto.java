package fooddelivery.dao.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import fooddelivery.entity.Address;

/**
 * AddressInfoDto contains address details.
 *
 */
public class AddressInfoDto {

	@JsonCreator
	public AddressInfoDto(@JsonProperty("addressId") int addressId,
			@JsonProperty("addressName") String addressName,
			@JsonProperty("nearyByAddresses") List<AddressInfoDto> nearByAddresses) {
		this.addressId = addressId;
		this.addressName = addressName;
		this.nearByAddresses = nearByAddresses;
	}
	
	public AddressInfoDto(int addressId,String addressName){
		this(addressId, addressName, null);
	}

	private int addressId;

	private String addressName;

	List<AddressInfoDto> nearByAddresses;

	public int getAddressId() {
		return addressId;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	public String getAddressName() {
		return addressName;
	}

	public void setAddressName(String addressName) {
		this.addressName = addressName;
	}

	public List<AddressInfoDto> getNearByAddresses() {
		return nearByAddresses;
	}

	public void setNearByAddresses(List<AddressInfoDto> nearByAddresses) {
		this.nearByAddresses = nearByAddresses;
	}
}
