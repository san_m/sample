package fooddelivery.dao.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderInfoDto {
	
	private UserInfoDto userInfo;
	
	private List<CartInfoDto> restaurantInfoDto;

	@JsonCreator
	public OrderInfoDto(@JsonProperty("user") UserInfoDto userInfo, 
				@JsonProperty("cart") List<CartInfoDto> restaurantInfoDto) {
		this.userInfo = userInfo;
		this.restaurantInfoDto = restaurantInfoDto;
	}

	public UserInfoDto getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfoDto userInfo) {
		this.userInfo = userInfo;
	}

	public List<CartInfoDto> getRestaurantInfoDto() {
		return restaurantInfoDto;
	}

	public void setRestaurantInfoDto(List<CartInfoDto> restaurantInfoDto) {
		this.restaurantInfoDto = restaurantInfoDto;
	}
	
	

}
