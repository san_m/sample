package fooddelivery.dao.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * RestaurantInfoDto contains Restaurant Details.
 *
 */
public class RestaurantInfoDto {

	private String restaurantName;

	private int restaurantid;

	private String dishName;

	private int dishid;

	private double price;

	private double rating;
	
	@JsonCreator
	public RestaurantInfoDto(@JsonProperty("restaurantName") String restaurantName, 
			@JsonProperty("restaurantid") int restaurantid,
			@JsonProperty("dishName") String dishName,
			@JsonProperty("dishid") int dishid,
			@JsonProperty("price") double price,
			@JsonProperty("rating") double rating) {
		super();
		this.restaurantName = restaurantName;
		this.restaurantid = restaurantid;
		this.dishName = dishName;
		this.dishid = dishid;
		this.price = price;
		this.rating = rating;
	}

	public String getRestaurantName() {
		return restaurantName;
	}

	public void setRestaurantName(String restaurantName) {
		this.restaurantName = restaurantName;
	}

	public int getRestaurantid() {
		return restaurantid;
	}

	public void setRestaurantid(int restaurantid) {
		this.restaurantid = restaurantid;
	}

	public String getDishName() {
		return dishName;
	}

	public void setDishName(String dishName) {
		this.dishName = dishName;
	}

	public int getDishid() {
		return dishid;
	}

	public void setDishid(int dishid) {
		this.dishid = dishid;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}
}
