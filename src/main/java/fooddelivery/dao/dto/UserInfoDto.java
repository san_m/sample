package fooddelivery.dao.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;;

/**
 * UserInfoDto contains user details.
 *
 */
public class UserInfoDto {

	@JsonCreator
	public UserInfoDto(@JsonProperty("userId") int userId, 
			@JsonProperty("userName") String userName, 
			@JsonProperty("userAddressId") int userAddressId,
			@JsonProperty("userAddress") String userAddress) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userAddress = userAddress;
		this.userAddressId = userAddressId;
	}

	private int userId;

	private String userName;

	private String userAddress;
	
	private int userAddressId;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}

	public int getUserAddressId() {
		return userAddressId;
	}

	public void setUserAddressId(int userAddressId) {
		this.userAddressId = userAddressId;
	}

}
