package fooddelivery.dao;

import fooddelivery.entity.User;

public interface UserInterface {
	
	public User getUser(int userId);
}
