package fooddelivery.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import fooddelivery.dao.OrderInterface;
import fooddelivery.entity.Order;
import fooddelivery.entity.OrderDetail;

public class OrderDaodummy implements OrderInterface{

	@Override
	public String saveOrder(Order orderInfo) {
		String orderId = "ORD_"+new Date().getTime();
		return orderId;
	}

	@Override
	public void saveOrderDetail(OrderDetail orderDetailInfo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<OrderDetail> getOrderInfo(String orderNo) {
		// TODO Auto-generated method stub
		OrderDetail orderDetail = new OrderDetail(orderNo, 1, 1, 100.00, 1);
		return Arrays.asList(orderDetail);
	}

}
