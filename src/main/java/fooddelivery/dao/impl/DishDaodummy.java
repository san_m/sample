package fooddelivery.dao.impl;

import java.util.ArrayList;
import java.util.List;

import fooddelivery.dao.DishInterface;
import fooddelivery.entity.Dish;

public class DishDaodummy implements DishInterface{

	List<Dish> dishList = new ArrayList<>();
	Dish dish1 = new Dish(1,"Lemon-Rosemary Vegetable");
	Dish dish2 = new Dish(2,"Orange-Rosemary Salad");
	Dish dish3 = new Dish(3,"Lemon-Rosemary Squide");
	Dish dish4 = new Dish(4,"Orange-Rosemary Shrimp");
	Dish dish5 = new Dish(5,"Lemon-Rosemary Prawn");
	Dish dish6 = new Dish(6,"Orange-Rosemary Shrimp");
	Dish dish7 = new Dish(7,"Lemon-Rosemary Prawn");
	Dish dish8 = new Dish(8,"Lemon-Rosemary Vegetable");

	public DishDaodummy(){
		dishList.add(dish1);
		dishList.add(dish2);
		dishList.add(dish3);
		dishList.add(dish4);
		dishList.add(dish5);
		dishList.add(dish6);
		dishList.add(dish7);
		dishList.add(dish8);
	}

	/**
	 * getDish method to return the target dish details.
	 * 
	 */
	@Override
	public Dish getDish(int dishId) {
		return dishList.get(dishId);
	}

	/**
	 * getDishDetails method to return the list of dish details.
	 * 
	 */
	@Override
	public List<Dish> getDishDetails(List<Integer> dishIdlist) {
		List<Dish> result = new ArrayList<>();
		for(Dish irwd :dishList ){
			for(Integer rId : dishIdlist) {
				if(rId.equals(irwd.getDishId())){
					result.add(irwd);
				}
			}
		}
		return result;
	}

}
