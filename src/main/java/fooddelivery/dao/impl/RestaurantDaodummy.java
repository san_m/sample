package fooddelivery.dao.impl;

import java.util.ArrayList;
import java.util.List;

import fooddelivery.dao.RestaurantInterface;
import fooddelivery.entity.Restaurant;

public class RestaurantDaodummy implements RestaurantInterface{

	List<Restaurant> restaurantList = new ArrayList<>();
	Restaurant Restaurant1 = new Restaurant(1,"Tibb's Frankie",1,4.0);
	Restaurant Restaurant2 = new Restaurant(2,"Tibb's Frankie1",1,2.0);
	Restaurant Restaurant3 = new Restaurant(3,"Tibb's Franki2",1,0.0);


	Restaurant Restaurant4 = new Restaurant(4,"Tibb's Frankie3",2,1.0);
	Restaurant Restaurant5 = new Restaurant(5,"Tibb's Frankie4",2,3.0);

	Restaurant Restaurant6 = new Restaurant(6,"Tibb's Frankie5",3,5.0);
	Restaurant Restaurant7 = new Restaurant(7,"Tibb's Frankie6",3,2.0);
	Restaurant Restaurant8 = new Restaurant(8,"Tibb's Frankie7",3,1.0);
	
	Restaurant Restaurant9 = new Restaurant(9,"Tibb's Frankie8",0,0.0);
	Restaurant Restaurant10 = new Restaurant(10,"Tibb's Frankie9",0,4.0);

	public RestaurantDaodummy(){
		restaurantList.add(Restaurant1);
		restaurantList.add(Restaurant2);
		restaurantList.add(Restaurant3);

		restaurantList.add(Restaurant4);
		restaurantList.add(Restaurant5);
		restaurantList.add(Restaurant6);

		restaurantList.add(Restaurant7);
		restaurantList.add(Restaurant8);
		restaurantList.add(Restaurant9);
		restaurantList.add(Restaurant10);
		
	}

	/**
	 * getRestaurantByAddress method to return the Restaurant details of target address.
	 */
	@Override
	public List<Restaurant> getRestaurantByAddress(int desAddressId) {
		List<Restaurant> restaurantListByDes = new ArrayList<>();
		for(Restaurant res : restaurantList){
			if(res.getRestaurantAddrID()==desAddressId){
				restaurantListByDes.add(res);
			}
		}
		return restaurantListByDes;
	}

	/**
	 * getRestaurants method to return the Restaurant details.
	 */
	@Override
	public List<Restaurant> getRestaurants(List<Integer> restaurantId){
		List<Restaurant> restaurantList = new ArrayList<>();
		Restaurant restaurant = new Restaurant(1,"Rama",1,4.0);
		restaurantList.add(restaurant);
		return restaurantList;
	}

}
