package fooddelivery.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fooddelivery.dao.RestaurantWithDishInterface;
import fooddelivery.entity.RestaurantWithDish;

public class RestaurantWithDishDaodummy implements RestaurantWithDishInterface{

	List<RestaurantWithDish> restaurantWithDishList = new ArrayList<>();
	//Tibb's Frankie
	RestaurantWithDish restaurantWithDish1 = new RestaurantWithDish(100,1,1);
	RestaurantWithDish restaurantWithDish2 = new RestaurantWithDish(101,1,2);

	//Tibb's Frankie1
	RestaurantWithDish restaurantWithDish3 = new RestaurantWithDish(102,2,3);
	RestaurantWithDish restaurantWithDish4 = new RestaurantWithDish(103,2,4);

	//Tibb's Frankie2
	RestaurantWithDish restaurantWithDish5 = new RestaurantWithDish(104,3,5);
	RestaurantWithDish restaurantWithDish6 = new RestaurantWithDish(105,3,4);
	
	RestaurantWithDish restaurantWithDish7 = new RestaurantWithDish(106,4,5);
	RestaurantWithDish restaurantWithDish8 = new RestaurantWithDish(107,5,3);
	
	RestaurantWithDish restaurantWithDish9 = new RestaurantWithDish(108,6,5);
	RestaurantWithDish restaurantWithDish10 = new RestaurantWithDish(109,7,6);
	
	RestaurantWithDish restaurantWithDish11 = new RestaurantWithDish(110,8,7);
	RestaurantWithDish restaurantWithDish12 = new RestaurantWithDish(111,9,8);

	public RestaurantWithDishDaodummy(){
		restaurantWithDishList.add(restaurantWithDish1);
		restaurantWithDishList.add(restaurantWithDish2);
		restaurantWithDishList.add(restaurantWithDish3);
		restaurantWithDishList.add(restaurantWithDish4);
		restaurantWithDishList.add(restaurantWithDish5);
		restaurantWithDishList.add(restaurantWithDish6);
		restaurantWithDishList.add(restaurantWithDish7);
		restaurantWithDishList.add(restaurantWithDish8);
		restaurantWithDishList.add(restaurantWithDish9);
		restaurantWithDishList.add(restaurantWithDish10);
		restaurantWithDishList.add(restaurantWithDish11);
		restaurantWithDishList.add(restaurantWithDish12);

	}

	/**
	 * getRestaurantWithDishInfo method return the dish details of target Restaurants
	 */
	@Override
	public Map<Integer, Integer> getRestaurantWithDishInfo(List<Integer> restaurantIdDetails) {
		Map<Integer, Integer> resultMap = new HashMap<Integer,Integer>();
		for(RestaurantWithDish i :restaurantWithDishList ){
			if(resultMap.containsKey(i.getRestaurantId())){
				resultMap.put(i.getRestaurantId(),i.getDishId());
			}
		}
		return resultMap;
	}

	/**
	 * getRestaurantWithDishDetails method return the dish details.
	 */
	@Override
	public List<RestaurantWithDish> getRestaurantWithDishDetails(List<Integer> restaurantIdDetails) {
		List<RestaurantWithDish> result = new ArrayList<>();
		for(RestaurantWithDish irwd :restaurantWithDishList ){
			for(Integer rId : restaurantIdDetails) {
				if(rId.equals(irwd.getRestaurantId())){
					result.add(irwd);
				}
			}
		}
		return result;
	}

}