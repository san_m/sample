package fooddelivery.dao.impl;

import java.util.ArrayList;
import java.util.List;

import fooddelivery.dao.PriceInterface;
import fooddelivery.entity.Price;

public class PriceDaodummy implements PriceInterface{
	
	List<Price> priceList = new ArrayList<>();
	Price price1 = new Price(1,100,100.0d);
	Price price2 = new Price(1,101,220.0d);
	Price price3 = new Price(1,102,250.0d);
	Price price4 = new Price(1,103,100.0d);
	Price price5 = new Price(1,104,150.0d);
	Price price6 = new Price(1,105,150.0d);
	Price price7 = new Price(1,106,150.0d);
	Price price8 = new Price(1,107,150.0d);
	Price price9 = new Price(1,108,150.0d);
	Price price10 = new Price(1,109,100.0d);
	Price price11= new Price(1,110,100.0d);
	Price price12= new Price(1,111,200.0d);
	
	
	public PriceDaodummy(){
		priceList.add(price1);
		priceList.add(price2);
		priceList.add(price3);
		priceList.add(price4);
		priceList.add(price5);
		priceList.add(price6);
		priceList.add(price7);
		priceList.add(price8);
		priceList.add(price9);
		priceList.add(price10);
		priceList.add(price11);
		priceList.add(price12);
	}

	/**
	 * getPriceDetails method to return price details.
	 */
	@Override
	public List<Price> getPriceDetails(List<Integer> resWithDishId) {
		List<Price> result = new ArrayList<>();
		for(Price irwd :priceList ){
			for(Integer rId : resWithDishId) {
				if(rId.equals(irwd.getResWithDishId())){
					result.add(irwd);
				}
			}
		}
		return result;
	}
 
}
