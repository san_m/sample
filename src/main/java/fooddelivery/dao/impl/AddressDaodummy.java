package fooddelivery.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fooddelivery.dao.AddressInterface;
import fooddelivery.entity.Address;
import fooddelivery.entity.Dish;

public class AddressDaodummy implements AddressInterface{

	List<Address> addressList = new ArrayList<>();
	Address address0 = new Address(0,"AGS",null);
	Address address1 = new Address(1,"Navalur",null);
	Address address2 = new Address(2,"Sipcot",null);
	Address address3 = new Address(3,"Padur",null);
	
	Map<Integer, List<Integer>> nearByDestiantionsMap = new HashMap<>();
	
	public AddressDaodummy(){
		addressList.add(address0);
		addressList.add(address1);
		addressList.add(address2);
		addressList.add(address3);
		
		nearByDestiantionsMap.put(address0.getAddressId(), 
				 Arrays.asList(address1.getAddressId()));
		
		nearByDestiantionsMap.put(address1.getAddressId(), 
				 Arrays.asList(address0.getAddressId(), 
						 	   address2.getAddressId()));
		
		nearByDestiantionsMap.put(address2.getAddressId(), 
				 Arrays.asList(address1.getAddressId(), 
						 	   address3.getAddressId()));
		
		nearByDestiantionsMap.put(address3.getAddressId(), 
				 Arrays.asList(address2.getAddressId()));
	}
	/**
	 * getAddress method to return the corresponding address details
	 */
	@Override
	public Address getAddress(int addressId){
		for(Address address :addressList ){
			if(address.getAddressId() == addressId ){
				return address;
			}
		}
		return null;
	}

	/**
	 * getNearbyDestination method to return the corresponding near by address details
	 */
	@Override
	public List<Address> getNearbyDestination(int addressId) {
		List<Address> nearByLocations = new ArrayList<>();
		
		for(Integer nearByAddressId :nearByDestiantionsMap.get(addressId) ){
			nearByLocations.add(getAddress(nearByAddressId));
		}
		
		return nearByLocations;
	}

}
