package fooddelivery.dao;

import java.util.List;

import fooddelivery.entity.Price;

public interface PriceInterface {
	
	public List<Price> getPriceDetails(List<Integer> resWithDishId);
}
