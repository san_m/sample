package fooddelivery.dao;

import java.util.List;
import java.util.Map;

import fooddelivery.entity.RestaurantWithDish;

public interface RestaurantWithDishInterface {
  
	public Map<Integer,Integer> getRestaurantWithDishInfo(List<Integer> restaurantIdDetails);
	
	public List<RestaurantWithDish> getRestaurantWithDishDetails(List<Integer> restaurantIdDetails);
		
}
