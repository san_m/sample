package fooddelivery.dao;

import java.util.List;

import fooddelivery.entity.Restaurant;

public interface RestaurantInterface {
	
	public List<Restaurant> getRestaurantByAddress(int desAddressId);
	
	public List<Restaurant> getRestaurants(List<Integer> restaurantId);
	
	

}
