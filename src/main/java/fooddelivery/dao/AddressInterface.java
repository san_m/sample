package fooddelivery.dao;

import java.util.List;

import fooddelivery.entity.Address;

public interface AddressInterface {
	
	public Address getAddress(int addressId);
	
	public List<Address> getNearbyDestination(int addressId);
}
