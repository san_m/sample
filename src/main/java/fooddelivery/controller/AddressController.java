package fooddelivery.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fooddelivery.dao.AddressInterface;
import fooddelivery.dao.dto.AddressInfoDto;
import fooddelivery.dao.dto.FailureResponseDto;
import fooddelivery.dao.impl.AddressDaodummy;
import fooddelivery.entity.Address;

@Path("/addressController")
public class AddressController {
	
	public AddressController(AddressInterface addressInfo) {
		this.addressInfo = addressInfo;
	}
	
	public AddressController() {
		this(new AddressDaodummy());
	}
	

	AddressInterface addressInfo;
	
	@GET
	@Path("/getNearAddress/{id}")
	public String getNearByDestinations(@PathParam("id") int userDesID) throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		try {
			Address userAddress = addressInfo.getAddress(userDesID);
			List<Address> nearByAddresses = addressInfo.getNearbyDestination(userDesID);
			
			if(!nearByAddresses.contains(userAddress)){
				nearByAddresses.add(userAddress);
			}
			
			List<AddressInfoDto> nearByAddressInfoDto = nearByAddresses.stream()
			.map(nearByAddress -> 
				new AddressInfoDto(nearByAddress.getAddressId(), nearByAddress.getAddressName()))
			.collect(Collectors.toList());
			
			AddressInfoDto addressDto = new AddressInfoDto(userAddress.getAddressId()
					,userAddress.getAddressName(), nearByAddressInfoDto);
			
			return mapper.writeValueAsString(addressDto);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapper.writeValueAsString(new FailureResponseDto("Error", 
				"Retrieving nearby locations failed. Please try again later :("));
	}
}
