package fooddelivery.controller;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fooddelivery.dao.DishInterface;
import fooddelivery.dao.PriceInterface;
import fooddelivery.dao.RestaurantInterface;
import fooddelivery.dao.RestaurantWithDishInterface;
import fooddelivery.dao.dto.FailureResponseDto;
import fooddelivery.dao.dto.RestaurantInfoDto;
import fooddelivery.dao.impl.DishDaodummy;
import fooddelivery.dao.impl.PriceDaodummy;
import fooddelivery.dao.impl.RestaurantDaodummy;
import fooddelivery.dao.impl.RestaurantWithDishDaodummy;
import fooddelivery.entity.Dish;
import fooddelivery.entity.Price;
import fooddelivery.entity.Restaurant;
import fooddelivery.entity.RestaurantWithDish;
import fooddelivery.utils.PriceFluctuator;

@Path("/restaurantController")
public class RestaurantController {


	private RestaurantInterface restaurant;

	private DishInterface dish;

	private RestaurantWithDishInterface restaurantWithDishInterface;

	private PriceInterface price;
     
	private PriceFluctuator priceFluctuator;


	public RestaurantController(RestaurantInterface restaurant, DishInterface dish,
			RestaurantWithDishInterface restaurantWithDishInterface, PriceInterface price, 
			PriceFluctuator priceFluctuator) {
		super();
		this.restaurant = restaurant;
		this.dish = dish;
		this.restaurantWithDishInterface = restaurantWithDishInterface;
		this.price = price;
		this.priceFluctuator = priceFluctuator;
	}

	public RestaurantController() {
			this(new RestaurantDaodummy(),new DishDaodummy(),new RestaurantWithDishDaodummy(),
				new PriceDaodummy(), new PriceFluctuator());
	}


	@GET
	@Path("/getRestaurantsInfo/{key}")
	public String getRestaurantsInfo(@PathParam("key") int selectedAddressId) throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		try {
			List<Restaurant> restaurantListOfSelectedAddr = restaurant.getRestaurantByAddress(selectedAddressId);
			List<Integer> restaurantIdsList = new ArrayList<>();
			for(Restaurant res : restaurantListOfSelectedAddr){
				restaurantIdsList.add(res.getRestaurantId());
			}
			//This map contains restaurantId and DishId
			List<RestaurantWithDish>  restaurantWithDishList = restaurantWithDishInterface.getRestaurantWithDishDetails(restaurantIdsList);
	
			List<Integer> restaurantWithDishMapId = restaurantWithDishList.stream()
					.map(RestaurantWithDish::getResWithDishId)
					.collect(Collectors.toList());
	
			List<Price> priceList = price.getPriceDetails(restaurantWithDishMapId);
			if (null != this.priceFluctuator) {
				priceList = this.priceFluctuator.flutuatePrice(priceList);
			}
			List<Integer> dishIdlist = restaurantWithDishList.stream()
					.map(RestaurantWithDish::getDishId)
					.collect(Collectors.toList());
			List<Dish> dishList = dish.getDishDetails(dishIdlist);
	
			//Form PriceMap (restaurantWithDishMapId , Price)
			Map<Integer,Price> priceMap = new HashMap<>();
			for(Price pr : priceList){
				priceMap.put(pr.getResWithDishId(), pr);
			}
	
			//Form dishMap (DishId , dishName)
			Map<Integer, Dish> dishMap = new HashMap<>();
			Map<Integer, Restaurant> restaurantMap = new HashMap<>();
			for(RestaurantWithDish restaurantWithDish : restaurantWithDishList){
				for(Dish dish : dishList) {
					if(restaurantWithDish.getDishId() == dish.getDishId())
						dishMap.put(restaurantWithDish.getResWithDishId(), dish);
				}
				for(Restaurant restaurant : restaurantListOfSelectedAddr){
					if(restaurantWithDish.getRestaurantId() == restaurant.getRestaurantId()) {
						restaurantMap.put(restaurantWithDish.getResWithDishId(), restaurant);
					}
				}
			}
			List<RestaurantInfoDto> result = populateRestarauntInfoDTO(restaurantWithDishList, restaurantMap, dishMap, priceMap);
			return mapper.writeValueAsString(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapper.writeValueAsString(new FailureResponseDto("Error", 
				"Not able to retrieve restaurant details. Please try again later :("));
	}



	private List<RestaurantInfoDto> populateRestarauntInfoDTO(List<RestaurantWithDish> restaurantWithDishList,
			Map<Integer, Restaurant> restaurantMap, Map<Integer, Dish> dishMap, Map<Integer, Price> priceMap) {
		List<RestaurantInfoDto> restaurantInfoDtoList = new ArrayList<>();
		Restaurant restaurant;
		Dish dish;
		Price price;
		for(RestaurantWithDish restaurantWithDish : restaurantWithDishList){
			restaurant = restaurantMap.get(restaurantWithDish.getResWithDishId());
			dish = dishMap.get(restaurantWithDish.getResWithDishId());
			price = priceMap.get(restaurantWithDish.getResWithDishId());
			
			RestaurantInfoDto restaurantInfoDto = new RestaurantInfoDto(
														restaurant.getRestaurantName(), 
														restaurant.getRestaurantId(),
														dish.getDishName(),
														dish.getDishId(),
														price.getPrice(),
														restaurant.getRating());
			
			restaurantInfoDtoList.add(restaurantInfoDto);
		}
		
		return restaurantInfoDtoList;
	}

}
