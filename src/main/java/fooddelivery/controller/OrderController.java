package fooddelivery.controller;

import java.util.Date;
import java.util.List;

import javax.ws.rs.POST;
import javax.ws.rs.Path;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fooddelivery.dao.OrderInterface;
import fooddelivery.dao.dto.CartInfoDto;
import fooddelivery.dao.dto.FailureResponseDto;
import fooddelivery.dao.dto.OrderInfoDto;
import fooddelivery.dao.dto.UserInfoDto;
import fooddelivery.dao.impl.OrderDaodummy;
import fooddelivery.entity.Order;
import fooddelivery.entity.OrderDetail;

@Path("/OrderController")
public class OrderController {
	
	OrderInterface orderInterface;
	
	public OrderController(OrderInterface orderInterface) {
		super();
		this.orderInterface = orderInterface;
	}
	
	public OrderController(){
		 this(new OrderDaodummy());
	}
	
	@POST
	@Path("/placeOrder")
	public String placeOrder(String orderInfo) throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			
			OrderInfoDto orderInfoDto = mapper.readValue(orderInfo, OrderInfoDto.class);
			UserInfoDto userInfo = orderInfoDto.getUserInfo();
			List<CartInfoDto> restaurantInfoDto = orderInfoDto.getRestaurantInfoDto();
			Order order = new Order(userInfo.getUserId(),userInfo.getUserAddressId(),new Date());
			String orderNo = orderInterface.saveOrder(order);
			if(null != orderNo && !"".equals(orderNo)) {
				
				for(CartInfoDto cartInfoDto : restaurantInfoDto){
					OrderDetail orderdetail = new OrderDetail(orderNo,cartInfoDto.getRestaurantInfo().getRestaurantid(),
							cartInfoDto.getRestaurantInfo().getDishid(), cartInfoDto.getRestaurantInfo().getPrice(), cartInfoDto.getQuantity());
					orderInterface.saveOrderDetail(orderdetail);
				}
				return "{\"Status\":\"Success\", \"OrderNo\":\""+orderNo+"\"}";
			}else {
				return mapper.writeValueAsString(new FailureResponseDto("Error", 
						"Order placement failed. Please try again later :("));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return mapper.writeValueAsString(new FailureResponseDto("Error", 
				"Order placement failed. Please try again later :("));
	}
	
	@POST
	@Path("/getOrderInfo")
	public String getOrderInfo(String orderNo) throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(this.orderInterface.getOrderInfo(orderNo));
		
	}
}
