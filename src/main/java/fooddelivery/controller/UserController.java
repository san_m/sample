package fooddelivery.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fooddelivery.dao.AddressInterface;
import fooddelivery.dao.UserInterface;
import fooddelivery.dao.dto.FailureResponseDto;
import fooddelivery.dao.dto.UserInfoDto;
import fooddelivery.dao.impl.AddressDaodummy;
import fooddelivery.dao.impl.UserDaodummy;
import fooddelivery.entity.Address;
import fooddelivery.entity.User;


@Path("/userController")
public class UserController {

	UserInterface userDao;

	AddressInterface addressInfo;

	public UserController(UserInterface userDao, AddressInterface addressInfo) {
		this.userDao = userDao;
		this.addressInfo = addressInfo;
	}
	
	public UserController() {
		this(new UserDaodummy(), new AddressDaodummy());
	}
	
	

	@GET
	@Path("/user/{id}")
	public String getUserDetail(@PathParam("id") int userId) throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		try {
			User user = userDao.getUser(userId);
			Address address = addressInfo.getAddress(user.getUserAddressId());
			UserInfoDto userDto = new UserInfoDto(user.getUserId(),user.getUserName(),
					user.getUserAddressId(),address.getAddressName());
			return mapper.writeValueAsString(userDto);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapper.writeValueAsString(new FailureResponseDto("Error", 
				"Not able to retrieve user details. Please try again later :("));
	}
}
