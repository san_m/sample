package fooddelivery.entity;

public class RestaurantWithDish {
	
	private int resWithDishId;
	
	private int restaurantId;
	
	private int dishId;
	
	public RestaurantWithDish(int resWithDishId, int restaurantId, int dishId) {
		super();
		this.resWithDishId = resWithDishId;
		this.restaurantId = restaurantId;
		this.dishId = dishId;
	}

	public int getResWithDishId() {
		return resWithDishId;
	}

	public void setResWithDishId(int resWithDishId) {
		this.resWithDishId = resWithDishId;
	}

	public int getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(int restaurantId) {
		this.restaurantId = restaurantId;
	}

	public int getDishId() {
		return dishId;
	}

	public void setDishId(int dishId) {
		this.dishId = dishId;
	}


}
