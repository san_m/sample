package fooddelivery.entity;

public class Restaurant {
	
	private int restaurantId;
	
	private String restaurantName;
	
	private int restaurantAddrID;
	
	private double rating;
	
	public Restaurant(int restaurantId, String restaurantName, int restaurantAddrID, double rating) {
		super();
		this.restaurantId = restaurantId;
		this.restaurantName = restaurantName;
		this.restaurantAddrID = restaurantAddrID;
		this.rating = rating;
	}
	
	public int getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(int restaurantId) {
		this.restaurantId = restaurantId;
	}

	public String getRestaurantName() {
		return restaurantName;
	}

	public void setRestaurantName(String restaurantName) {
		this.restaurantName = restaurantName;
	}

	public int getRestaurantAddrID() {
		return restaurantAddrID;
	}

	public void setRestaurantAddrID(int restaurantAddrID) {
		this.restaurantAddrID = restaurantAddrID;
	}
	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

}
