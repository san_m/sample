package fooddelivery.entity;

import java.util.List;

public class Address {

	private int addressId;

	private String addressName;

	List<Address> nearByLocations;

	public Address(int addressId, String addressName, List<Address> nearByLocations) {
		super();
		this.addressId = addressId;
		this.addressName = addressName;
		this.nearByLocations = nearByLocations;
	}

	public int getAddressId() {
		return addressId;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	public String getAddressName() {
		return addressName;
	}

	public void setAddressName(String addressName) {
		this.addressName = addressName;
	}

	public List<Address> getNearByLocations() {
		return nearByLocations;
	}

	public void setNearByLocations(List<Address> nearByLocations) {
		this.nearByLocations = nearByLocations;
	}
}
