package fooddelivery.entity;

public class Dish {
	
	private int dishId;
	
	private String dishName;

	public Dish(int dishId, String dishName) {
		super();
		this.dishId = dishId;
		this.dishName = dishName;
	}

	public int getDishId() {
		return dishId;
	}

	public void setDishId(int dishId) {
		this.dishId = dishId;
	}

	public String getDishName() {
		return dishName;
	}

	public void setDishName(String dishName) {
		this.dishName = dishName;
	}


}
