package fooddelivery.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderDetail {
  
	private String orderDetailId;
	
	private String orderId;
	
	private int restaurantId;
	
	private int dishId;
	
	private double price;
	
	private int qty;

	@JsonCreator
	public OrderDetail( @JsonProperty("orderId") String orderId, 
			@JsonProperty("restaurantId") int restaurantId, 
			@JsonProperty("dishId") int dishId, 
			@JsonProperty("price") double price, 
			@JsonProperty("qty") int qty) {
		super();
		this.orderId = orderId;
		this.restaurantId = restaurantId;
		this.dishId = dishId;
		this.price = price;
		this.qty = qty;
	}

	public String getOrderDetailId() {
		return orderDetailId;
	}

	public void setOrderDetailId(String orderDetailId) {
		this.orderDetailId = orderDetailId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public int getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(int restaurantId) {
		this.restaurantId = restaurantId;
	}

	public int getDishId() {
		return dishId;
	}

	public void setDishId(int dishId) {
		this.dishId = dishId;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

}
