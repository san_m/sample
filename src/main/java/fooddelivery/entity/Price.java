package fooddelivery.entity;

public class Price {

	private int priceId;
	
	private int resWithDishId;
	
	private double price;

	public Price(int priceId, int resWithDishId, double price) {
		super();
		this.priceId = priceId;
		this.resWithDishId = resWithDishId;
		this.price = price;
	}

	public int getPriceId() {
		return priceId;
	}

	public void setPriceId(int priceId) {
		this.priceId = priceId;
	}

	public int getResWithDishId() {
		return resWithDishId;
	}

	public void setResWithDishId(int resWithDishId) {
		this.resWithDishId = resWithDishId;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
}
