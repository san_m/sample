package fooddelivery.entity;

import java.util.Date;

public class Order {
	
	private int orderId;
	
	private int userId;
	
	private int userAddressId;
	
	private Date orderDate;

	public Order(int userId, int userAddressId, Date orderDate) {
		super();
		this.userId = userId;
		this.userAddressId = userAddressId;
		this.orderDate = orderDate;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public int getUserAddressId() {
		return userAddressId;
	}

	public void setUserAddressId(int userAddressId) {
		this.userAddressId = userAddressId;
	}

}
