package fooddelivery.entity;

public class User {
	
	private int userId;
	
	private String userName;
	
	private int userAddressId;
	
	public User(int userId, String userName, int userAddressId) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userAddressId = userAddressId;
	}
	
	public int getUserId() {
		return userId;
	}
	
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public String getUserName() {
		return userName;
	}
	
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public int getUserAddressId() {
		return userAddressId;
	}
	
	public void setUserAddressId(int userAddressId) {
		this.userAddressId = userAddressId;
	}

}
