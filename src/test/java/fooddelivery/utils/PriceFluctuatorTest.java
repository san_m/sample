package fooddelivery.utils;

import java.util.List;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import fooddelivery.entity.Price;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

@RunWith(MockitoJUnitRunner.class)
public class PriceFluctuatorTest {

	PriceFluctuator priceFluctuator;
	
	DynamicConfigReader dynamicConfigReader;
	
	double priceFactor = 1.5;
	
	@Before
	public void setUp(){
		Properties properties = new Properties();
		properties.setProperty("BreakFast.Interval", "06,10");
		properties.setProperty("Lunch.Interval", "12,15");
		properties.setProperty("Dinner.Interval", "19,22");
		properties.setProperty("Sunday.Lunch.Factor", priceFactor+"");
		dynamicConfigReader = new DynamicConfigReader(properties);
		priceFluctuator = new PriceFluctuator(dynamicConfigReader);
	}
	
	@Test
	public void fluctuatePriceTest() {
		LocalDateTime localDate = LocalDateTime.parse("2020-08-23T13:00");
		priceFluctuator.setLocalDateTime(localDate);
		
		double givenPrice = 100;
		Price price = new Price(1, 1, givenPrice);
		List<Price> priceList = priceFluctuator.flutuatePrice(Arrays.asList(price));
		
		assertNotNull(priceList);
		assertThat(priceList.size(), is(1));
		Price actaulPrice = priceList.get(0);
		assertNotNull(actaulPrice);
		assertThat(actaulPrice.getPrice(), is(givenPrice*priceFactor));
	}
	
	@Test
	public void fluctuatePriceNoChangeTest() {
		LocalDateTime localDate = LocalDateTime.parse("2020-08-22T13:00");
		priceFluctuator.setLocalDateTime(localDate);
		
		double givenPrice = 100;
		Price price = new Price(1, 1, givenPrice);
		List<Price> priceList = priceFluctuator.flutuatePrice(Arrays.asList(price));
		
		assertNotNull(priceList);
		assertThat(priceList.size(), is(1));
		Price actaulPrice = priceList.get(0);
		assertNotNull(actaulPrice);
		assertThat(actaulPrice.getPrice(), is(givenPrice));
	}
}
