package fooddelivery.utils;

import java.io.IOException;
import java.util.Properties;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

@RunWith(MockitoJUnitRunner.class)
public class DynamicConfigReaderTest {

	@Test
	public void readPropertyUsingFileTest() throws IOException{
		DynamicConfigReader dynamicConfigReader = new DynamicConfigReader("src/main/resources/price_fluctuator.properties");
		String value = dynamicConfigReader.readProperty("Key");
		//File loaded without exception is enough for verification
	}
	
	@Test
	public void readPropertyUsingProperties() throws IOException {
		Properties properties = new Properties();
		properties.setProperty("Key", "Value");
		DynamicConfigReader dynamicConfigReader = new DynamicConfigReader(properties);
		String value = dynamicConfigReader.readProperty("Key");
		assertThat(value, is("Value"));
	}
	
}
