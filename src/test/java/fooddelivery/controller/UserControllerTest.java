package fooddelivery.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fooddelivery.dao.AddressInterface;
import fooddelivery.dao.UserInterface;
import fooddelivery.dao.dto.FailureResponseDto;
import fooddelivery.dao.dto.UserInfoDto;
import fooddelivery.entity.Address;
import fooddelivery.entity.User;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {

	UserController userController;
	ObjectMapper objectMapper;
	User user;
	Address userAddress;
	
	@Mock
	UserInterface userDao;
	
	@Mock
	AddressInterface addressDao;
	
	@Before
	public void setUp() {
		user = new User(1, "user", 1);
		userAddress = new Address(1, "Address1", null);
		userController = new UserController(userDao, addressDao);
		objectMapper = new ObjectMapper();
	}
	
	@Test
	public void getUserDetailTest() throws JsonMappingException, JsonProcessingException {
		when(addressDao.getAddress(user.getUserAddressId())).thenReturn(userAddress);
		when(userDao.getUser(user.getUserId())).thenReturn(user);
		
		String response = userController.getUserDetail(user.getUserId());
		UserInfoDto actualUser = objectMapper.readValue(response, UserInfoDto.class);
		
		assertNotNull(actualUser);
		assertThat(actualUser.getUserId(), is(user.getUserId()));
		assertThat(actualUser.getUserName(), is(user.getUserName()));
		assertThat(actualUser.getUserAddressId(), is(user.getUserAddressId()));
		assertThat(actualUser.getUserAddress(), is(userAddress.getAddressName()));
	}
	
	@Test
	public void getUserDetailFailTest() throws JsonMappingException, JsonProcessingException {
		when(userDao.getUser(user.getUserId())).thenThrow(new RuntimeException("Unit test failure case"));
		
		String response = userController.getUserDetail(user.getUserId());
		FailureResponseDto failureMessage = (new ObjectMapper()).readValue(response, FailureResponseDto.class);
		
		//verify
		assertNotNull(failureMessage);
		assertThat(failureMessage.getStatus(), is("Error"));
		assertThat(failureMessage.getMessage(),
				is("Not able to retrieve user details. Please try again later :("));
	}
}
