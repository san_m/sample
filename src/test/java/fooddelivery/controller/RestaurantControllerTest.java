package fooddelivery.controller;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fooddelivery.dao.DishInterface;
import fooddelivery.dao.PriceInterface;
import fooddelivery.dao.RestaurantInterface;
import fooddelivery.dao.RestaurantWithDishInterface;
import fooddelivery.dao.dto.FailureResponseDto;
import fooddelivery.dao.dto.RestaurantInfoDto;
import fooddelivery.entity.Dish;
import fooddelivery.entity.Price;
import fooddelivery.entity.Restaurant;
import fooddelivery.entity.RestaurantWithDish;
import fooddelivery.utils.PriceFluctuator;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RestaurantControllerTest {

	RestaurantController restaurantController;
	
	RestaurantWithDish restaurantWithDish;
	Restaurant restaurant;
	Price price;
	Dish dish;
	
	ObjectMapper mapper;
	
	@Mock
	RestaurantInterface restaurantDao;
	
	@Mock 
	DishInterface dishDao;
	
	@Mock
	RestaurantWithDishInterface restaurantWithDishDao;
	
	@Mock
	PriceInterface priceDao;
	
	@Mock
	PriceFluctuator priceFluctuaotor;
	
	@Before
	public void setUp() {
		restaurant = new  Restaurant(1, "restaurantName", 2, 3.0d);
		dish = new Dish(4, "dishName");
		restaurantWithDish = new RestaurantWithDish(5, restaurant.getRestaurantId(), dish.getDishId());
		price = new Price(6, restaurantWithDish.getResWithDishId(), 7.00d);
		mapper = new ObjectMapper();
		restaurantController =  new RestaurantController(restaurantDao, dishDao, 
				restaurantWithDishDao, priceDao, priceFluctuaotor);
	}
	
	@Test
	public void getRestaurantsInfoTest() throws JsonMappingException, JsonProcessingException {
		RestaurantInfoDto expectedOutput = new RestaurantInfoDto(
				restaurant.getRestaurantName(), 
				restaurant.getRestaurantId(),
				dish.getDishName(),
				dish.getDishId(),
				price.getPrice(),
				restaurant.getRating());
		
		when(restaurantDao.getRestaurantByAddress(anyInt())).thenReturn(listOf(restaurant));
		when(restaurantWithDishDao.getRestaurantWithDishDetails(any())).thenReturn(listOf(restaurantWithDish));
		when(dishDao.getDishDetails(any())).thenReturn(listOf(dish));
		when(priceDao.getPriceDetails(any())).thenReturn(listOf(price));
		when(priceFluctuaotor.flutuatePrice(any())).thenReturn(listOf(price));
		
		String response = restaurantController.getRestaurantsInfo(restaurant.getRestaurantId());
		List<RestaurantInfoDto> actualOutputList = 
				mapper.readValue(response, mapper.getTypeFactory().constructCollectionType(List.class, RestaurantInfoDto.class));
		
		assertNotNull(actualOutputList);
		assertThat(actualOutputList.size(), is(1));
	
		RestaurantInfoDto actualOutput = actualOutputList.get(0);
		
		assertThat(actualOutput.getDishid(), is(expectedOutput.getDishid()));
		assertThat(actualOutput.getDishName(), is(expectedOutput.getDishName()));
		assertThat(actualOutput.getPrice(), is(expectedOutput.getPrice()));
		assertThat(actualOutput.getRating(), is(expectedOutput.getRating()));
		assertThat(actualOutput.getRestaurantid(), is(expectedOutput.getRestaurantid()));
		assertThat(actualOutput.getRestaurantName(), is(expectedOutput.getRestaurantName()));
	}
	
	@Test
	public void getRestaurantsInfoFailTest() throws JsonMappingException, JsonProcessingException {
		
		when(restaurantDao.getRestaurantByAddress(anyInt())).thenThrow(new RuntimeException("Unit test failure case"));
		
		String response = restaurantController.getRestaurantsInfo(restaurant.getRestaurantId());
		FailureResponseDto failureMessage = (new ObjectMapper()).readValue(response, FailureResponseDto.class);
		
		//verify
		assertNotNull(failureMessage);
		assertThat(failureMessage.getStatus(), is("Error"));
		assertThat(failureMessage.getMessage(),
				is("Not able to retrieve restaurant details. Please try again later :("));
	}
	
	private static <T> List<T> listOf(T object) {
		return Arrays.asList(object);
	}
}
