package fooddelivery.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fooddelivery.dao.dto.AddressInfoDto;
import fooddelivery.dao.dto.FailureResponseDto;
import fooddelivery.dao.impl.AddressDaoImpl;
import fooddelivery.entity.Address;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AddressControllerTest {

	AddressController addressController;
	
	@Mock AddressDaoImpl mockAddressDao;
	List<Address> nearByAddresses = new ArrayList<>();
	Address userAddress = new Address(1, "Address1", null);
	Address nearByAddress1 = new Address(2, "nearByAddress", null);
	
	@Before
	public void setup() {
		nearByAddresses.add(nearByAddress1);
		userAddress = new Address(1, "Address1", nearByAddresses);
		addressController = new AddressController(mockAddressDao);
	}
	
	@Test
	public void getNearByDestinationsTest() throws JsonMappingException, JsonProcessingException{
		
		List<AddressInfoDto> nearByAddressDtoList = Arrays.asList(
				new AddressInfoDto(nearByAddress1.getAddressId(), nearByAddress1.getAddressName()));
		AddressInfoDto expectedAddress = new AddressInfoDto(userAddress.getAddressId(), userAddress.getAddressName(),
				nearByAddressDtoList);
		
		when(mockAddressDao.getNearbyDestination(anyInt())).thenReturn(nearByAddresses);
		when(mockAddressDao.getAddress(anyInt())).thenReturn(userAddress);
		
		String actualOutput = addressController.getNearByDestinations(1);
		AddressInfoDto actualAddress = (new ObjectMapper()).readValue(actualOutput, AddressInfoDto.class);
		
		//verify
		assertNotNull(actualAddress);
		assertThat(actualAddress.getAddressId(), is(expectedAddress.getAddressId()));
		assertThat(actualAddress.getAddressName(), is(expectedAddress.getAddressName()));
		assertNotNull(actualAddress.getNearByAddresses());
		// user address will also be included in the nearby address
		assertThat(actualAddress.getNearByAddresses().size(), is(nearByAddressDtoList.size()+1));
		for (AddressInfoDto actualNearByAddress : actualAddress.getNearByAddresses()) {
			for (AddressInfoDto expcetedNearByAddress : nearByAddressDtoList) {
				if(expcetedNearByAddress.getAddressId() == actualNearByAddress.getAddressId()) {
					assertThat(actualNearByAddress.getAddressName(), is(expcetedNearByAddress.getAddressName()));
					assertNull(actualNearByAddress.getNearByAddresses());
				}
			}
		}
	}
	
	@Test
	public void getNearByDestinationsFailTest() throws JsonMappingException, JsonProcessingException{
		
		List<AddressInfoDto> nearByAddressDtoList = Arrays.asList(
				new AddressInfoDto(nearByAddress1.getAddressId(), nearByAddress1.getAddressName()));
		AddressInfoDto expectedAddress = new AddressInfoDto(userAddress.getAddressId(), userAddress.getAddressName(),
				nearByAddressDtoList);
		
		when(mockAddressDao.getNearbyDestination(anyInt())).thenThrow(new RuntimeException("Unit Test failure case"));
		String actualOutput = addressController.getNearByDestinations(1);
		FailureResponseDto failureMessage = (new ObjectMapper()).readValue(actualOutput, FailureResponseDto.class);
		
		//verify
		assertNotNull(failureMessage);
		assertThat(failureMessage.getStatus(), is("Error"));
		assertThat(failureMessage.getMessage(),
				is("Retrieving nearby locations failed. Please try again later :("));
	}
}
