package fooddelivery.controller;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fooddelivery.dao.OrderInterface;
import fooddelivery.dao.dto.CartInfoDto;
import fooddelivery.dao.dto.FailureResponseDto;
import fooddelivery.dao.dto.OrderInfoDto;
import fooddelivery.dao.dto.RestaurantInfoDto;
import fooddelivery.dao.dto.UserInfoDto;
import fooddelivery.entity.OrderDetail;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderControllerTest {
	
	OrderController orderController;
	OrderInfoDto orderInfoDto;
	UserInfoDto userInfoDto;
	RestaurantInfoDto restaurantInfoDto;
	CartInfoDto cartInfoDto;
	ObjectMapper mapper;
	String request;
	OrderDetail orderDetail;
	
	@Mock
	OrderInterface orderDao;
	
	@Before
	public void setUp() throws JsonProcessingException{
		userInfoDto = new UserInfoDto(1, "userName", 1, "userAddress");
		restaurantInfoDto = new RestaurantInfoDto("restaurantName", 1, 
												  "dishName", 1, 100d, 1d);
		cartInfoDto = new CartInfoDto(restaurantInfoDto, 10);
		orderInfoDto = new OrderInfoDto(userInfoDto, Arrays.asList(cartInfoDto));
		orderController = new OrderController(orderDao);
		mapper = new ObjectMapper();
		orderDetail = new OrderDetail("1", 2, 3, 4.0d, 5);
		request = mapper.writeValueAsString(orderInfoDto);
	}
	
	@Test
	public void placeOrderTest() throws JsonProcessingException {
		String orderNo = "orderNo";
		String expectedResponse = "{\"Status\":\"Success\", \"OrderNo\":\""+orderNo+"\"}";
		when(orderDao.saveOrder(any())).thenReturn(orderNo);
		
		String actualResponse = orderController.placeOrder(request);
		
		assertThat(actualResponse, is(expectedResponse));
	}
	
	@Test
	public void placeOrderFail1Test() throws JsonProcessingException {
		String orderNo = "";
		String expectedResponse = "{Status:Error, Message : \"Order placement failed. Try again later :(\"}";
		when(orderDao.saveOrder(any())).thenReturn(orderNo);
		
		String actualResponse = orderController.placeOrder(request);
		FailureResponseDto failureMessage = (new ObjectMapper()).readValue(actualResponse, FailureResponseDto.class);
		
		//verify
		assertNotNull(failureMessage);
		assertThat(failureMessage.getStatus(), is("Error"));
		assertThat(failureMessage.getMessage(),
				is("Order placement failed. Please try again later :("));
	}
	
	@Test
	public void placeOrderFail2Test() throws JsonProcessingException {
		when(orderDao.saveOrder(any())).thenThrow(new RuntimeException("Unit test failure case"));
		
		String actualResponse = orderController.placeOrder(request);
		FailureResponseDto failureMessage = (new ObjectMapper()).readValue(actualResponse, FailureResponseDto.class);
		
		//verify
		assertNotNull(failureMessage);
		assertThat(failureMessage.getStatus(), is("Error"));
		assertThat(failureMessage.getMessage(),
				is("Order placement failed. Please try again later :("));
	}
	
	@Test
	public void getOrderInfo() throws JsonProcessingException {
		when(orderDao.getOrderInfo(any())).thenReturn(Arrays.asList(orderDetail));
		
		String response = orderController.getOrderInfo("orderNo");
		
		assertNotNull(response);
		
		List<OrderDetail> orderDetails = mapper.readValue(response, 
				mapper.getTypeFactory().constructCollectionType(List.class, OrderDetail.class));
		
		OrderDetail actaulOrderDetail = orderDetails.get(0);
		
		assertThat(actaulOrderDetail.getOrderId(), is(orderDetail.getOrderId()));
		assertThat(actaulOrderDetail.getRestaurantId(), is(orderDetail.getRestaurantId()));
		assertThat(actaulOrderDetail.getDishId(), is(orderDetail.getDishId()));
		assertThat(actaulOrderDetail.getPrice(), is(orderDetail.getPrice()));
		assertThat(actaulOrderDetail.getQty(), is(orderDetail.getQty()));
	}
}
